package com.Ascend;

import java.util.HashSet;

public class Hashset {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashSet<Integer> evenNumber = new HashSet<>();

        // Using add() method
        evenNumber.add(1000);
        evenNumber.add(2000);
        evenNumber.add(3000);
        System.out.println("HashSet: " + evenNumber);

        HashSet<Integer> numbers = new HashSet<>();
        
        // Using addAll() method
        numbers.addAll(evenNumber);
        numbers.add(5000);
        System.out.println("New HashSet: " + numbers);

	}

}
